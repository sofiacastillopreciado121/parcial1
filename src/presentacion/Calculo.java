package presentacion;
import logica.Persona;
import logica.Hombre;
import logica.Mujer;

public class Calculo {
	public static void main(String[] args) {
		Persona hombre = new Hombre(74.0, 180.0, 19);
		Persona mujer = new Mujer(60.0, 162.0, 17);
		
		double tmbHombre = hombre.calcularTMB();
		double tmbMujer = mujer.calcularTMB();
		
		System.out.println("TMB para el hombre:" + tmbHombre + "kcal/kg/h");
		System.out.println("TMB para la mujer:" + tmbMujer + "kcal/kg/h");
	}
}
